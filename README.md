# Global Application Manifest Repository

Contains all the application helm charts for clusters with the `global` label.

### Directory Structure

```
apps/
└── {namespace}/
    └── {application-name}/
        ├── templates/
        ├── Chart.yaml        
        └── values.yaml
values/
└── {cluster}/
    └── {application-name}/
        └── values.yaml
└── shared/
    └── {application-name}/
        └── values.yaml
```